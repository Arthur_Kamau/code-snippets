import React, { Component } from 'react'
import colorAppStore from "../store/store";
import { changeColor } from '../actions/actions';


// remeber  actions dispatcher store view
// when you press a button it invokes an action
// the action updates the dispatcher that updates the state that emits an event
// we listen for the events and fetch the new state
// when existing the view we unlisten , remove memory leaks


// in this example when you tap on a button it will change the box color.
export interface ColorComponentProps {
    
}
 
export interface ColorComponentState {
    color: string
}
 
class ColorComponent extends React.Component<ColorComponentProps, ColorComponentState> {
    constructor(props: ColorComponentProps) {
        super(props);
        this.state = { color: colorAppStore.getActiveColor() };
    }


    componentDidMount() {
        colorAppStore.on("storeUpdated", this.updateBackgroundColor);
    }

    componentWillUnmount() {
        colorAppStore.removeListener("storeUpdated", this.updateBackgroundColor);
    }

    updateBackgroundColor = () => {
        this.setState({color: colorAppStore.getActiveColor()})
    }

    render() { 
        return (   
            <div>
        <button onClick={(e: any)=>{changeColor("red")}}>Red</button>
        <button onClick={(e: any)=>{changeColor("green")}}>Green</button>
        <button onClick={(e: any)=>{changeColor("blue")}}>Blue</button>
        <div className="color-container" style={{ width:`500px`,height:`200px`,marginTop:`50px`, backgroundColor: this.state.color}}/> 
        </div>
        );
    }
}
 
export default ColorComponent;


// import React from "react";
//

// export default class ColorComponent extends React.Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             
//         }
//     }

   ;

//     render() {
//         return (
//           
//         );
//     }
// }