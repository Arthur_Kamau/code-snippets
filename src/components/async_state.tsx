import React, { Component } from 'react'

// interface
export interface AsyncExampleProps {

}

export interface AsyncExampleState {
   
    data: string,
    
}

class AsyncExample extends React.Component<AsyncExampleProps, AsyncExampleState> {
    constructor(props: AsyncExampleProps) {
        super(props);
        this.state = {
           data: ""
        };
    }


    componentDidMount() {
        var axios = require('axios');

        axios.get('https://www.cloudflare.com/cdn-cgi/trace')
        .then((response : any) => {
    
            console.log(response.data);

            this.setState({data: response.data})
        })
        .catch((error : any) => {
          console.log(error);
        });
    }  


  
    render() {
        return (

        <React.Fragment>{ this.state.data.length == 0 ? " i  am empty" : this.state.data}</React.Fragment>

        );
    }
}

export default AsyncExample;