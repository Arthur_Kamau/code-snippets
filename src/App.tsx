import React from 'react';

import AsyncExample from './components/async_state';
import ColorComponent from './components/flux_colors';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* asynchrously update state */}
       {/* <AsyncExample></AsyncExample> */}
       {/* using flux to update state */}
       <ColorComponent></ColorComponent>
      </header>
    </div>
  );
}

export default App;
